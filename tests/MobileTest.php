<?php

namespace Tests;

use Mockery as m;
use PHPUnit\Framework\TestCase;
use App\Mobile;
use App\Carrier;

class MobileTest extends TestCase
{
	
	/** @test */
	public function it_returns_null_when_name_empty()
	{
		$provider = new Carrier();
		$mobile = new Mobile($provider);

		$this->assertNull($mobile->makeCallByName(''));
	}

	/** @test */
	public function it_return_call(){
		$provider = new Carrier();
		$mobile = new Mobile($provider);

		$this->assertEquals("App\Call", get_class($mobile->makeCallByName('Luis')));
	}

	/** @test */
	public function it_returns_null_when_name_not_found()
	{
		$provider = new Carrier();
		$mobile = new Mobile($provider);

		$this->assertNull($mobile->makeCallByName('Gonzalo'));
	}

	/** @test */
	public function it_returns_true_when_send_a_sms()
	{
		$provider = new Carrier();
		$mobile = new Mobile($provider);

		$this->assertTrue($mobile->sendSms("949001087", "Hola soy Luis!"));
	}
}