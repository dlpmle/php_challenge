<?php

namespace App\Services;

use App\Contact;


class ContactService
{
	private const arrayContact = ["Luis", "Enrique"];
	//private const arrayNumbers = [949001087, 999999888];

	public static function findByName($name)
	{	
		if(\in_array($name, ContactService::arrayContact)){
			return new Contact($name);
		}
		// queries to the db
		return null;
	}

	public static function validateNumber(string $number): bool
	{
		// logic to validate numbers
		return strlen($number) == 9;
	}
}