<?php

namespace App;

use App\Interfaces\CarrierInterface;
use App\Services\ContactService;
use App\Sms;

class Mobile
{

	protected $provider;
	
	function __construct(CarrierInterface $provider)
	{
		$this->provider = $provider;
	}


	public function makeCallByName($name = '')
	{
		if( empty($name) ) return;

		$contact = ContactService::findByName($name);
		if(\is_null($contact)) return null;

		$this->provider->dialContact($contact);

		return $this->provider->makeCall();
	}

	public function sendSms($number, $text){
		if(ContactService::validateNumber($number)){
			$sms = new Sms($number, $text);
			return $sms->send();
		}
		return false;
	}
}
