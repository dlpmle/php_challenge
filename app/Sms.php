<?php
namespace App;

class Sms{

    private $number;
    private $text;

    function __construct($number, $text){
        $this->number = $number;
        $this->text = $text;
    }

    public function send(){
        if(strlen($this->text) > 160){
            return false;
        }
        return true;
    }

}