<?php

namespace App;
use App\Interfaces\CarrierInterface;
use App\Contact;
use App\Call;

class Carrier implements CarrierInterface
{

    public function dialContact(Contact $contact){
        return null;
    }

	public function makeCall(): Call{
        return new Call();
    }
}